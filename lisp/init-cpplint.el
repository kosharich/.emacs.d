(defun my:flymake-google-init ()
    (require 'flymake-google-cpplint)
    (custom-set-variables
     '(flymake-google-cpplint-verbose "3")
     '(flymake-google-cpplint-filter "-whitespace/braces,-legal/copyright")
     '(flymake-google-cpplint-command "/Library/Python/2.7/site-packages/cpplint.py"))
    (flymake-google-cpplint-load)
    )
