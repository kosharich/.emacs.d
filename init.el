;; Local settings (for proxy, e.g.)

(add-to-list 'load-path "~/.emacs.d/local")
(add-to-list 'load-path "~/.emacs.d/lisp")

(load "local-before.el" t)

;; Packages

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages")
			 ("marmalade" . "http://marmalade-repo.org/packages/")
			 ("melpa_milkbox" . "http://melpa.milkbox.net/packages/")
                         ("melpa" . "http://melpa.org/packages/")))

(package-initialize)

(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)
(require 'yasnippet)
(yas-global-mode 1)

(defun my:ac-c-header-init ()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers)
  )
(require 'iedit)

;; UI

(load-theme 'warm-night t)

(setq inhibit-startup-message t)

(menu-bar-mode 0)
(tool-bar-mode 0)

(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

;; (set-frame-parameter (selected-frame) 'fullscreen 'maximized)

;; Python

(add-hook 'python-mode-hook
          (lambda ()
            (setq tab-width 4)
            (setq python-indent 4)))

;; C/C++

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

(c-add-style "my-style"
             '("bsd"
               (indent-tabs-mode . nil)
               (c-basic-offset . 4)))

(add-hook 'c++-mode-hook
          (lambda ()
            (c-set-style "my-style")))

(add-hook 'c++-mode-hook 'my:ac-c-header-init)
(add-hook 'c-mode-hook 'my:ac-c-header-init)

(load "init-cpplint.el" t)
(add-hook 'c++-mode-hook 'my:flymake-google-init)

;; Modes

(global-auto-revert-mode t)

(semantic-mode 1)

(defun my:add-semantic-to-autocomplete()
  (add-to-list 'ac-sources 'ac-source-semantic)
  )

(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)
(global-semantic-idle-scheduler-mode 1)

;; Fix iedit bug in Mac
(define-key global-map (kbd "C-c ;") 'iedit-mode)


;; Generic editing

(setq tab-stop-list (number-sequence 4 120 4))
(setq-default indent-tabs-mode nil)

(delete-selection-mode 1)

(global-set-key (kbd "C-'") 'next-error)
(global-set-key (kbd "C-M-'") 'previous-error)
(global-set-key (kbd "C-=") 'er/expand-region)

;; Projectile

(projectile-global-mode)

;; Navigation

(ido-mode t)

(setq ido-enable-flex-matching t)

;; Shell

(add-hook 'shell-mode
	  '(lambda ()
	     (local-set-key (kbd "C-a") 'comint-bol)))

;; Windows

(when (string-equal system-type "windows-nt")
  (progn
    (setq cygwin-bin "c:\\cygwin64\\bin")
    (setenv "PATH" (concat cygwin-bin ";"))
    (setq exec-path '(cygwin-bin))))

(setq null-device "/dev/null")

;; Heplers

(defun swap-windows ()
  "If you have 2 windows, it swaps them."
  (interactive)
  (cond ((/= (count-windows) 2)
         (message "You need exactly 2 windows to do this."))
        (t
         (let* ((w1 (nth 0 (window-list)))
                (w2 (nth 1 (window-list)))
                (b1 (window-buffer w1))
                (b2 (window-buffer w2))
                (s1 (window-start w1))
                (s2 (window-start w2)))
           (set-window-buffer w1 b2)
           (set-window-buffer w2 b1)
           (set-window-start w1 s2)
           (set-window-start w2 s1))))
  (other-window 1))
